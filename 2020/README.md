# Advent of Code 2020

This is my attempt at Advent of Code 2020 using Rust
Some of these problems are also attempted in other programming languages,
most notably in Bash and/or C as when I feel it will be interesting.


## Usage

This project uses the cargo-aoc crate for helping with the Advent of Code.
To use it, run `cargo install cargo-aoc` in the terminal and then:

* To execute: `$ cargo aoc`
* To benchmark: `$ cargo aoc bench`
* To test: `$ cargo test`
* To execute a specific day: `$ cargo aoc -d {day} -p {part}`

## License

This project is licensed under the GNU GPLv3+
