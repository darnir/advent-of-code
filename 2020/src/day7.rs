use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::convert::From;

#[derive(Debug, Default, Eq, PartialEq, Hash, Clone)]
struct Bag(String);

#[derive(Debug, Default)]
struct LuggageRules {
    reverse: HashMap<Bag, Vec<Bag>>,
    direct: HashMap<Bag, Vec<(usize, Bag)>>,
}

impl From<&str> for Bag {
    fn from(a: &str) -> Bag {
        Self(a.trim().trim_end_matches('s').into())
    }
}

#[aoc_generator(day7)]
fn generator(input: &str) -> LuggageRules {
    let mut rules: LuggageRules = Default::default();
    for rule in input.lines() {
        let (outer_bag, inner_bags) = rule
            .splitn(2, "contain")
            .collect_tuple()
            .expect("Error in splitting rule");
        let outer_bag = Bag::from(outer_bag);
        let direct_list = rules.direct.entry(outer_bag.clone()).or_default();
        for (num, colour) in inner_bags.split(',').map(|l| {
            l.trim()
                .trim_end_matches('.')
                .trim_end_matches('s')
                .splitn(2, ' ')
                .collect_tuple()
                .unwrap()
        }) {
            rules
                .reverse
                .entry(colour.into())
                .or_default()
                .push(outer_bag.clone());
            if num != "no" {
                direct_list.push((num.parse().unwrap(), colour.into()));
            }
        }
    }
    rules
}

fn find_bag_sets(rules: &LuggageRules, target: &Bag, possible: &mut HashSet<Bag>) -> usize {
    if let Some(bags) = rules.reverse.get(target) {
        for bag in bags {
            if possible.contains(bag) {
                continue;
            }
            possible.insert(bag.clone());
            find_bag_sets(rules, bag, possible);
        }
    }
    possible.len()
}

#[aoc(day7, part1)]
fn part1(rules: &LuggageRules) -> usize {
    let target = Bag::from("shiny gold bag");
    let mut possible = HashSet::new();
    find_bag_sets(rules, &target, &mut possible)
}

fn find_included_bags(rules: &LuggageRules, target: &Bag) -> usize {
    rules
        .direct
        .get(target)
        .unwrap()
        .iter()
        .map(|(num, colour)| num * (find_included_bags(rules, colour) + 1))
        .sum()
}

#[aoc(day7, part2)]
fn part2(rules: &LuggageRules) -> usize {
    let target = Bag::from("shiny gold bag");
    find_included_bags(rules, &target)
}

#[test]
fn test_part1() {
    let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
    let rules = generator(input);
    let mut possible = HashSet::new();
    assert_eq!(part1(&rules), 4);
    assert_eq!(
        find_bag_sets(&rules, &Bag::from("faded blue bags"), &mut possible),
        7
    );
}

#[test]
fn test_part2() {
    let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
    let rules = generator(input);
    assert_eq!(part2(&rules), 32);
}
