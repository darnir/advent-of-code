use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::collections::HashMap;

#[aoc_generator(day4)]
fn parse(input: &str) -> Vec<HashMap<String, String>> {
    input
        .split("\n\n")
        .map(|inp| {
            inp.split_whitespace()
                .flat_map(|v| v.split(':').map(|s| s.to_owned()))
                .tuples::<(_, _)>()
                .collect::<HashMap<_, _>>()
        })
        .collect::<Vec<_>>()
}

#[aoc(day4, part1)]
fn try1(passports: &[HashMap<String, String>]) -> usize {
    passports
        .iter()
        .filter(|m| m.len() == 8 || (m.len() == 7 && m.get("cid").is_none()))
        .count()
}

fn is_valid_year(year: &str, min: u16, max: u16) -> bool {
    if let Ok(v) = year.parse::<u16>() {
        v >= min && v <= max
    } else {
        false
    }
}

fn is_valid(passport: &HashMap<String, String>) -> bool {
    passport
        .iter()
        .take_while(|(key, value)| match key.as_ref() {
            "byr" => is_valid_year(value, 1920, 2002),
            "iyr" => is_valid_year(value, 2010, 2020),
            "eyr" => is_valid_year(value, 2020, 2030),
            "hgt" => {
                let x = value.split_at(value.len() - 2);
                if let Ok(height) = x.0.parse::<u8>() {
                    match x.1 {
                        "cm" => height >= 150 && height <= 193,
                        "in" => height >= 50 && height <= 76,
                        _ => false,
                    }
                } else {
                    false
                }
            }
            "hcl" => {
                let mut i = value.as_bytes().iter();
                i.next().unwrap() == &b'#'
                    && i.map(|c| c.is_ascii_hexdigit())
                        .fold(true, |acc, v| acc & v)
            }
            "ecl" => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&value.as_str()),
            "pid" => value.len() == 9 && value.parse::<u32>().is_ok(),
            _ => true,
        })
        .count()
        == passport.len()
    // for (key, value) in passport.iter() {
    //     match key.as_ref() {
    //         "byr" => value.len() == 4,
    //         _ => true,
    //     }
    // }
}

#[aoc(day4, part2)]
fn try1_part2(passports: &[HashMap<String, String>]) -> usize {
    passports
        .iter()
        .filter(|m| (m.len() == 8 || (m.len() == 7 && m.get("cid").is_none())) && is_valid(m))
        .count()
}

#[test]
fn part1() {
    let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";
    assert_eq!(try1(&parse(input)), 2);
}
