use aoc_runner_derive::{aoc, aoc_generator};

// #[aoc_generator(day5)]
// fn parse_day5(input: &str) -> Vec<(u64,u64)> {
//     input.lines().map(|s| {
//         let (row,col) = s.split_at(7);
//         let row = row.chars().map(|c| {
//             match c {
//                 'F' => '0',
//                 'B' => '1',
//                 _ => unreachable!(),
//             }
//         }).collect::<String>();
//         let col = match col {
//             "LLL" => 0,
//             "LLR" => 1,
//             "LRL" => 2,
//             "LRR" => 3,
//             "RLL" => 4,
//             "RLR" => 5,
//             "RRL" => 6,
//             "RRR" => 7,
//             _ => unreachable!(),
//         };
//         (u8::from_str_radix(&row, 2).unwrap() as u64, col)
//     }).collect::<Vec<_>>()
// }

// #[aoc(day5, part1)]
// fn part1(input: &Vec<(u64, u64)>) -> u64 {
//     input.iter().map(|(row, column)| ((row * 8)+column)).max().unwrap()
// }

// #[aoc(day5, part2)]
// fn part2(input: &Vec<(u64, u64)>) -> u64 {
//     let mut seats = input.iter().map(|(row, column)| ((row * 8)+column)).collect::<Vec<_>>();
//     seats.sort();
//     let mut last = seats[0] - 1;
//     *seats.iter().skip_while(|s| {last+=1; **s == last}).next().unwrap() - 1
// }

/*
 * even when writing the first implementation, I wanted to directly parse it as binary rather than
 * convert to string and then parse to binary. This changes the generator function, so I can't
 * enable both for a comparison.
 *
 * It took me an embarrasingly long time to identify that the whole number is
 * binary encoded. row*8+column just gives me the decimal representation of the 10-bit binary
 * value.
 *
 * But once I saw that, the rest of the code became stupidly fast and simple. It's really wrong to
 * compare the performance of the two implementations since I sort in the generator in the new
 * implementation. Still, even the generator now runs faster than previously. I'm leaving around my
 * stupidity as a comment above for posterity.
 */

#[aoc_generator(day5, insanity)]
fn parse_day5_better(input: &str) -> Vec<u32> {
    let mut seats = input
        .lines()
        .map(|l| {
            l.chars().fold(0, |acc, c| match c {
                'F' | 'L' => (acc << 1),
                'B' | 'R' => (acc << 1) | 1,
                _ => unreachable!(),
            })
        })
        .collect::<Vec<_>>();
    seats.sort_unstable();
    seats
}

#[aoc(day5, part1)]
fn part1(input: &[u32]) -> u32 {
    *input.last().unwrap()
}

#[aoc(day5, part2)]
fn part2(input: &[u32]) -> u32 {
    let mut last = input[0] - 1;
    input.iter().find(|s| {
        last += 1;
        **s != last
    });
    last
}

#[test]
fn test_part1() {
    assert_eq!(part1(&parse_day5_better("BFFFBBFRRR")), 567);
    assert_eq!(part1(&parse_day5_better("FFFBBBFRRR")), 119);
    assert_eq!(part1(&parse_day5_better("BBFFBBFRLL")), 820);
}
