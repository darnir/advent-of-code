use aoc_runner_derive::{aoc, aoc_generator};

/**
 * Initial Naive Attempt.
 *
 * An ugly first attempt at solving the problem. It works, but it is not
 * elegant.
 */

#[aoc_generator(day3)]
fn day2_input(input: &str) -> Vec<String> {
    input.split('\n').map(|s| s.to_owned()).collect()
}

#[aoc(day3, part1, try1)]
fn first_try(input: &[String]) -> usize {
    stepper(input, 3, 1)
}

#[aoc(day3, part2, try1)]
fn first_try_part2(input: &[String]) -> usize {
    stepper(input, 1, 1)
        * stepper(input, 3, 1)
        * stepper(input, 5, 1)
        * stepper(input, 7, 1)
        * stepper(input, 1, 2)
}

fn stepper(input: &[String], x: usize, y: usize) -> usize {
    let mut pos_x = 0;
    let width = input[0].len();
    let mut count = 0;
    for line in input.iter().step_by(y) {
        let item = line.as_bytes()[pos_x % width];
        if item == b'#' {
            count += 1;
        }
        pos_x += x;
    }
    count
}

/**
 * Using Rust's Iterators.
 *
 * An attempt at writing a more elegant solution using the power of iterators.
 */

#[aoc(day3, part1, Iterator)]
fn iter_try(input: &[String]) -> usize {
    stepper2(input, 3, 1)
}

#[aoc(day3, part2, Iterator)]
fn iter_try_part2(input: &[String]) -> usize {
    stepper2(input, 1, 1)
        * stepper2(input, 3, 1)
        * stepper2(input, 5, 1)
        * stepper2(input, 7, 1)
        * stepper2(input, 1, 2)
}

fn stepper2(input: &[String], x: usize, y: usize) -> usize {
    let width = input[0].len();
    input
        .iter()
        .enumerate()
        .step_by(y)
        .filter(|(n, l)| l.chars().nth(((n / y) * x) % width).unwrap() == '#')
        .count()
}

/**
 * Iterators with Indexing string.
 *
 * Going by the lesson learnt during Day 2, I thought that using the unsafe
 * direct indexing approach rather than UTF-8 handling will yield significant
 * gains. While it does improve performance, the overall speed of the iterator
 * based solutions continues to fair very poorly against the original, ugly
 * solution.
 */

#[aoc(day3, part1, unsafe_iter)]
fn unsafe_iter_try(input: &[String]) -> usize {
    stepper_unsafe(input, 3, 1)
}

#[aoc(day3, part2, unsafe_iter)]
fn unsafe_iter_try_part2(input: &[String]) -> usize {
    stepper_unsafe(input, 1, 1)
        * stepper_unsafe(input, 3, 1)
        * stepper_unsafe(input, 5, 1)
        * stepper_unsafe(input, 7, 1)
        * stepper_unsafe(input, 1, 2)
}

fn stepper_unsafe(input: &[String], x: usize, y: usize) -> usize {
    let width = input[0].len();
    input
        .iter()
        .enumerate()
        .step_by(y)
        .filter(|(n, l)| l.chars().nth(((n / y) * x) % width).unwrap() == '#')
        .count()
}

/**
 * Solution by u/liveliverDTN on r/rust.
 *
 * This is for comparison and studying other's code. Finally a solution
 * that uses iterators and actually has decent performance. Heck, it even
 * rivals the performance of the ugly, naive solution that was the fastest
 * until now.
 *
 * Looking at the code, it seems like the only difference between this and
 * my approach is in how the x-coordinate is handled. Instead of computing
 * the value based on the line number, this takes the smart approach and zips
 * an iterator. That makes a *HUGE* difference in performance.
 */

#[aoc(day3, part1, compare1)]
fn solve_part1(input: &[String]) -> usize {
    solve(&input, &[(3, 1)])
}

#[aoc(day3, part2, compare1)]
fn solve_part2(input: &[String]) -> usize {
    solve(&input, &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)])
}

fn solve(input: &[String], slopes: &[(usize, usize)]) -> usize {
    let line_width = input[0].len();
    slopes.iter().fold(1, |acc, &(x_step, y_step)| {
        acc * input
            .iter()
            .step_by(y_step)
            .zip((0..line_width).cycle().step_by(x_step))
            .skip(1)
            .filter(|&(line, x_pos)| line.get(x_pos..=x_pos).unwrap() == "#")
            .count()
    })
}

/**
 * Slight cleanup on previous solution.
 *
 * This is only a slight cleanup on the previous solution where instead of
 * using fold(), I decided to use the product() method and make the solution
 * a little more functional. No effect on performance expected or seen.
 *
 * I would like to study the difference in the generated code between the two
 * implementations though
 */

#[aoc(day3, part1, compare2)]
fn solve2_part1(input: &[String]) -> usize {
    solve2(&input, &[(3, 1)])
}

#[aoc(day3, part2, compare2)]
fn solve2_part2(input: &[String]) -> usize {
    solve2(&input, &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)])
}

fn solve2(input: &[String], slopes: &[(usize, usize)]) -> usize {
    let line_width = input[0].len();
    slopes
        .iter()
        .map(|&(x_step, y_step)| {
            input
                .iter()
                .step_by(y_step)
                .zip((0..line_width).cycle().step_by(x_step))
                .skip(1)
                .filter(|&(line, x_pos)| line.get(x_pos..=x_pos).unwrap() == "#")
                .count()
        })
        .product()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";

    #[test]
    fn part1() {
        let generated = day2_input(INPUT);
        assert_eq!(first_try(&generated), 7);
        assert_eq!(iter_try(&generated), 7);
    }

    #[test]
    fn part2() {
        let generated = day2_input(INPUT);
        assert_eq!(first_try_part2(&generated), 336);
        assert_eq!(iter_try_part2(&generated), 336);
    }
}
