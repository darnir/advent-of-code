# Advent of Code

This is my Advent of Code Repository of shame. It contains my partial attempts at completing the Advent of Code.

| Year | Language |
|------|----------|
| 2020 | Rust     |
| 2021 | Python   |
| 2022 | C++      |
