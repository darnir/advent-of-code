#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <set>
#include <vector>

inline uint32_t get_prio(char l) { return l - (islower(l) ? 96 : 38); }

uint32_t part1(std::string input) {
  std::ifstream file(input);
  std::string line;
  uint32_t priorities = 0;
  while (getline(file, line)) {
    assert(line.length() % 2 == 0);
    std::string second = line.substr(line.length() / 2, line.length() / 2);
    line.resize(line.length() / 2);
    assert(line.length() == second.length());
    std::set<char> c2(second.begin(), second.end());
    std::set<char> c1(line.begin(), line.end());
    std::vector<char> common;
    std::set_intersection(c1.begin(), c1.end(), c2.begin(), c2.end(),
                          std::back_inserter(common));
    assert(common.size() == 1);
    priorities += get_prio(common[0]);
  }
  return priorities;
}

uint32_t part2(std::string input) {
  std::ifstream file(input);
  std::string elf1, elf2, elf3;
  uint32_t priorities = 0;
  while (getline(file, elf1) && getline(file, elf2) && getline(file, elf3)) {
    std::set<char> e1(elf1.begin(), elf1.end());
    std::set<char> e2(elf2.begin(), elf2.end());
    std::set<char> e3(elf3.begin(), elf3.end());
    std::set<char> common;
    std::vector<char> badge;
    std::set_intersection(e1.begin(), e1.end(), e2.begin(), e2.end(),
                          std::inserter(common, common.begin()));
    std::set_intersection(common.begin(), common.end(), e3.begin(), e3.end(),
                          std::back_inserter(badge));
    priorities += get_prio(badge[0]);
  }

  return priorities;
}

int main(int argc, char *argv[]) {
  std::string input = "inputs/Day3.txt";
  if (argc > 1) {
    input = argv[1];
  }

  std::cout << part1(input) << std::endl;
  std::cout << part2(input) << std::endl;
}
