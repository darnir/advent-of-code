#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

int main() {
  std::ifstream file("./inputs/Day1.txt");
  std::vector<uint32_t> elves;
  std::string cals;
  uint32_t elfnum = 0;
  elves.push_back(0);
  if (!file.is_open()) {
    std::cerr << "Error opening file" << std::endl;
    return 1;
  }
  while (getline(file, cals)) {
    if (cals.empty()) {
      elfnum++;
      elves.push_back(0);
    } else {
      elves.back() += std::stoi(cals);
    }
  }
  std::sort(elves.begin(), elves.end());
  std::cout << elves.back() << std::endl;
  std::cout << elves[elfnum - 2] + elves[elfnum - 1] + elves[elfnum]
            << std::endl;
}
