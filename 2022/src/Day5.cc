#include <fstream>
#include <iostream>
#include <numeric>
#include <stack>
#include <vector>

template <typename T> void reverse(std::stack<T> &orig) {
  std::stack<T> reversed;
  while (!orig.empty()) {
    reversed.push(std::move(orig.top()));
    orig.pop();
  }
  orig = std::move(reversed);
}

int main(int argc, const char *argv[]) {
  std::string input = "inputs/Day5.txt";
  if (argc > 1) {
    input = argv[1];
  }
  std::ifstream file(input);
  std::string line;

  getline(file, line);
  const uint32_t num_stacks = (line.size() + 1) / 4;
  /* std::cout << "Num Stacks: " << num_stacks << std::endl; */

  std::vector<std::stack<char>> piles(num_stacks);

  do {
    std::cout << line << std::endl;
    for (uint32_t i = 0; i < num_stacks; i++) {
      const char crate = line[1 + (i * 4)];
      if (crate != ' ') {
        /* std::cout << "Adding crate [" << crate << "] to stack " << i <<
         * std::endl; */
        piles[i].push(crate);
      }
    }
  } while (getline(file, line) && line[1] != '1');

  for (auto &pile : piles) {
    reverse(pile);
  }
  std::vector<std::stack<char>> part2 = piles;

  uint32_t count, from, to;
  while (file >> line >> count >> line >> from >> line >> to) {
    // From needs to be 0-indexed
    from--;
    to--;

    /* std::cout << "[" << count << "] " << from << " -> " << to << std::endl;
     */
    std::stack<char> temp;
    for (uint32_t i = 0; i < count; i++) {
      char crate = piles[from].top();
      char crate2 = part2[from].top();
      /* std::cout << "Moving crate [" << crate << "]" << std::endl; */
      piles[to].push(crate);
      temp.push(crate2);
      piles[from].pop();
      part2[from].pop();
    }
    while (!temp.empty()) {
      char c = temp.top();
      /* std::cout << "Pushing " << c << " on stack " << to << std::endl; */
      part2[to].push(c);
      temp.pop();
    }
    std::cout << std::endl;
  }

  auto merge_top = [](std::string a, std::stack<char> &b) {
    return std::move(a) + b.top();
  };

  std::cout << std::accumulate(piles.begin(), piles.end(), std::string(""),
                               merge_top)
            << std::endl;
  std::cout << std::accumulate(part2.begin(), part2.end(), std::string(""),
                               merge_top)
            << std::endl;
}
