#include <fstream>
#include <iostream>

enum RPC { ROCK = 0, PAPER, SCISSORS };

RPC convert(char p) {
  RPC r;
  switch (p) {
  case 'A':
  case 'X':
    r = ROCK;
    break;
  case 'B':
  case 'Y':
    r = PAPER;
    break;
  case 'C':
  case 'Z':
    r = SCISSORS;
    break;
  default:
    exit(1);
  }
  return r;
}

RPC convert(char p1, char p2) {
  RPC r;
  RPC r1 = convert(p1);
  switch (p2) {
  case 'X':
    r = static_cast<RPC>((3 + r1 - 1) % 3);
    /* std::cout << "Must Lose. Opponent: " << r1 << " Mine: " << r <<
     * std::endl; */
    break;
  case 'Y':
    r = r1;
    /* std::cout << "Must Draw. Opponent: " << r1 << " Mine: " << r <<
     * std::endl; */
    break;
  case 'Z':
    r = static_cast<RPC>((r1 + 1) % 3);
    /* std::cout << "Must Win. Opponent: " << r1 << " Mine: " << r << std::endl;
     */
    break;
  default:
    exit(1);
  }
  return r;
}

inline uint32_t play(const RPC opp, const RPC mine) {
  switch ((3 + mine - opp) % 3) {
  case 0:
    return 3;
  case 1:
    return 6;
  default:
    return 0;
  }
}

uint32_t part1(const char p1, const char p2) {
  const RPC opp = convert(p1);
  const RPC mine = convert(p2);
  return mine + 1 + play(opp, mine);
}

uint32_t part2(const char p1, const char p2) {
  const RPC opp = convert(p1);
  const RPC mine = convert(p1, p2);
  return mine + 1 + play(opp, mine);
}

int main(int argc, char *argv[]) {
  std::string input = "inputs/Day2.txt";
  if (argc > 1) {
    input = argv[1];
  }

  std::ifstream file(input);
  std::string line;
  uint32_t score1 = 0;
  uint32_t score2 = 0;
  while (std::getline(file, line)) {
    score1 += part1(line[0], line[2]);
    score2 += part2(line[0], line[2]);
  }

  std::cout << "Part 1: " << score1 << std::endl;
  std::cout << "Part 2: " << score2 << std::endl;
  return 0;
}
