#include <fstream>
#include <iostream>

int main(int argc, char *argv[]) {
  std::string input = "inputs/Day4.txt";
  if (argc > 1) {
    input = argv[1];
  }

  std::ifstream file(input);
  uint32_t low1, low2, high1, high2;
  char sep;
  uint32_t count = 0;
  uint32_t any_overlap = 0;
  while (file >> low1 >> sep >> high1 >> sep >> low2 >> sep >> high2) {
    if ((low1 >= low2 && high1 <= high2) || (low1 <= low2 && high1 >= high2)) {
      // Part 1
      count++;
    } else if ((low1 >= low2 && low1 <= high2) ||
               (high1 >= low2 && high1 <= high2)) {
      // Part 2
      any_overlap++;
    }
  }
  std::cout << count << std::endl;
  std::cout << any_overlap + count << std::endl;
}
