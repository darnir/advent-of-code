import sys
import itertools



class Card:
    numbers: list[list[int]]
    hits: list[list[bool]]

    def __init__(self, input_str: str):
        self.numbers = [list(map(int, line.split())) for line in input_str.split("\n")]
        self.hits = [[False for i in range(5)] for j in range(5)]

    def mark(self, num: int):
        for x, y in itertools.product(range(5), range(5)):
            if self.numbers[x][y] == num:
                self.hits[x][y] = True
                break

    def wins(self):
        def get_column(arr, i):
            return [row[i] for row in arr]

        return any(sum(row) == 5 for row in self.hits) or any(
            sum(column) == 5 for column in [get_column(self.hits, j) for j in range(5)]
        )

    def score(self, last: int):
        total = 0
        for x, y in itertools.product(range(5), range(5)):
            if not self.hits[x][y]:
                total += self.numbers[x][y]
        return total * last

    def __repr__(self):
        string: str = ""
        for row, hits in zip(self.numbers, self.hits):
            for num, hit in zip(row, hits):
                string += "{:2}({:>1}) ".format(num, hit)
            string += "\n"
        return string


if __name__ == "__main__":
    file = open(sys.argv[1] if len(sys.argv) > 1 else "inputs/Day4.txt", "r")
    calls = [int(x) for x in file.readline().rstrip().split(",")]
    file.readline()
    all_cards = [Card(x) for x in file.read().split("\n\n")]
    first = False
    for n in calls:
        for card in list(all_cards):
            card.mark(n)
            if card.wins():
                if not first:
                    print(card.score(n))
                    first = True
                if len(all_cards) == 1:
                    print(card.score(n))
                all_cards.remove(card)

    print(str(all_cards))
