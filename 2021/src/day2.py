"""
Advent of Code 2021
Day 2: Dive!
"""

import sys

if __name__ == "__main__":
    x: int = 0
    y: int = 0
    aim: int = 0
    ny: int = 0
    with open(
        sys.argv[1] if len(sys.argv) > 1 else "inputs/Day2.txt", "r", encoding="utf-8"
    ) as file:
        for line in file:
            command, steps = line.split()
            steps = int(steps)
            if command == "forward":
                x += steps
                ny += aim * steps
            elif command == "up":
                y -= steps
                aim -= steps
            elif command == "down":
                y += steps
                aim += steps
            else:
                sys.exit(1)
    print(x * y)
    print(x * ny)
