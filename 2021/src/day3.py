"""
Advent of Code 2021
Day 3: Binary Diagnostic
"""

import sys
import operator


def common_bit(arr: list[int], comp):
    """
    Find the most common value for each bit in an array and return a decimal number corresponding
    to those bits
    """
    if len(arr) == 1:
        return arr[0]

    final = 0
    sums = [0] * BITS
    for num in arr:
        for i in range(BITS):
            sums[BITS - i - 1] += 1 if (num & (1 << i)) else 0

    for (i, value) in enumerate(sums):
        if comp(value, (len(arr) / 2)):
            final |= 1 << (len(sums) - i - 1)

    return final


def part1(arr):
    """
    Solve Part1: Commpute the Gamma and Epsilon Values
    """
    gamma = common_bit(arr, operator.ge)
    mask = 2**BITS - 1
    epsilon = ~gamma & mask
    return gamma * epsilon


def part2(arr):
    """
    Solve Part 2: Compute the Oxygen Generator Rating and CO2 Scrubber Rating
    Since the two work on difference arrays, we cannot simply compute the negation as in Part 1.
    This has to be independently solved
    """
    ogr = list(arr)
    csr = list(arr)

    for i in range(BITS):
        ogr_num = common_bit(ogr, operator.ge)
        csr_num = common_bit(csr, operator.lt)

        bitmask = 1 << (BITS - i - 1)

        ogr_test = ogr_num & bitmask
        csr_test = csr_num & bitmask
        ogr = [x for x in ogr if (x & bitmask) == ogr_test]
        csr = [x for x in csr if (x & bitmask) == csr_test]

    return ogr[0] * csr[0]


if __name__ == "__main__":
    with open(
        sys.argv[1] if len(sys.argv) > 1 else "inputs/Day3.txt", "r", encoding="utf-8"
    ) as file:
        data = file.readlines()
    BITS = len(data[0].rstrip())
    data = [int(x.rstrip(), 2) for x in data]
    print(part1(data))
    print(part2(data))
