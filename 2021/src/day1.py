"""
Advent of Code 2021
Day 1: Sonar Sweep
"""

import sys


def window(arr, start, size):
    """
    Compute sum of elements in a Window of given size
    """
    tot = 0
    while size:
        try:
            tot += arr[start + size - 1]
        except IndexError:
            tot = 0
        size -= 1
    return tot


if __name__ == "__main__":
    depths = []
    with open(
        sys.argv[1] if len(sys.argv) > 1 else "inputs/Day1.txt", "r", encoding="utf-8"
    ) as file:
        depths = file.readlines()
    depths = [int(x) for x in depths]
    window1 = window(depths, 0, 1)
    window3 = window(depths, 0, 3)
    count1: int = 0
    count3: int = 0
    for i in range(1, len(depths)):
        nw1 = window(depths, i, 1)
        nw3 = window(depths, i, 3)
        if nw1 > window1:
            count1 += 1
        if nw3 > window3:
            count3 += 1
        window1 = nw1
        window3 = nw3

    print(count1)
    print(count3)
